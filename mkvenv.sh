#!/usr/bin/env bash

unameOut="$(uname -s)"

case "${unameOut}" in
Linux*) machine=Linux ;;
Darwin*) machine=Mac ;;
*) machine="UNKNOWN" ;;
esac

# Determine OS dependent user config
if [ $machine = "Linux" ]; then
  BASH_RC=$HOME/.bashrc
elif [ $machine = "Mac" ]; then
  BASH_RC=$HOME/.bash_profile
fi

# Make sure pipenv, pyenv and direnv are installed
if ! hash pipenv 2>/dev/null; then
  echo "Make sure that pipenv is installed. Aborting!"
  exit 1
fi

if ! hash pyenv 2>/dev/null; then
  echo "Make sure that pyenv is installed. Aborting!"
  exit 1
fi

if ! hash direnv 2>/dev/null; then
  echo "Make sure that direnv is installed. Aborting!"
  exit 1
fi

# Add bash hooks and path modifications
if ! grep -q 'pyenv init -' $BASH_RC; then
  echo "Adding pyenv hook to $BASH_RC"
  echo 'eval "$(pyenv init -)"' >> $BASH_RC
fi

if ! grep -q 'PYENV_ROOT' $BASH_RC; then
  echo 'export PYENV_ROOT="$HOME/.pyenv"' >> $BASH_RC
  echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> $BASH_RC
fi

if ! grep -q 'direnv hook bash' $BASH_RC; then
  echo "Adding direnv hook to $BASH_RC"
  echo 'eval "$(direnv hook bash)"' >> $BASH_RC
fi

if ! grep -q 'PIPENV_VENV_IN_PROJECT="enabled"' $BASH_RC; then
  echo "Changing venv location to inside project folder."
  echo 'PIPENV_VENV_IN_PROJECT="enabled"' >> $BASH_RC
fi

# Restart you shell to pick up the changes
source $BASH_RC

# Determine Python version from Pipfile
if [ -f ./.python-version ]; then
  PYTHON_VERS=$(cat ./.python-version)
  echo "Python version from local file '.python_version': $PYTHON_VERS"
elif [ -f $HOME/.pyenv/version ]; then
  PYTHON_VERS=$(cat $HOME/.pyenv/version)
  echo "Python version from global '.python_version': $PYTHON_VERS"
  # Set global python version as local
  pyenv local $PYTHON_VERS
else
  printf "\nPlease configure and install a global Python version first.
         To do so, run first 'pyenv install x.y.z' where x is the Python version, y is
         the major and z the minor number, for example: 3.7.4. Then, configure the
         global Python version by running: 'pyenv global x.y.z\n"
  exit 1
fi

echo "Determined Python version: $PYTHON_VERS"

echo "Adding auto-activation hook (.envrc)"
# This path was added to the PYTHONPATH earlier.
# export PYTHONPATH=$(pipenv --venv)/lib/$PYTHON_VERS_DIR/site-packages
cat <<EOT > .envrc
layout_pipenv
EOT

# Enable auto-activation
direnv allow

# Set up pipenv
# PYTHON_VERS_DIR=$(echo python"$(pyenv version)" | sed 's/\(3.[0-9]\).*/\1/')
echo "Setting up virtual environment"
pipenv install --python $PYTHON_VERS --skip-lock
